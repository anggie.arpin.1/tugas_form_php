<?php
    
    if(isset($_POST["submit"])){
        $nama = $_POST["nama"];
        $mapel = $_POST["mapel"];
        $uts = $_POST["UTS"];
        $uas = $_POST["UAS"];
        $tugas = $_POST["tugas"];

        $nilaiuts = $uts  * 0.35;
        $nilaiuas = $uas * 0.5;
        $nilaitugas = $tugas * 0.15;

        $total = $nilaiuts + $nilaiuas +$nilaitugas;

        //Let's Grading
        if($total>=90 && $total <= 100){
            $grade = "A";
        }else if($total>70 && $total < 90){
            $grade = "B";
        }else if($total>50 && $total <= 70){
            $grade = "C";
        }else if($total<=50){
            $grade = "D";
        }
    }

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sistem Input Nilai</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
</head>
<body>
    <div class="container">
        <div class="row justify-content-center">
        <div class="border border-secondary rounded mt-3">
            <div class="container p-3 my-3 bg-dark text-white">
                <h1>Formulir Input Nilai Siswa</h1>
                <p>Silahkan masukkan nilai siswa berdasarkan formulir di bawah ini</p>
            </div>
            <form action="" method="post">
            <!-- Form nama -->
            <div class="form1">
                <label for="Nama">Nama :</label><br>
                <input type="text" class="form-control" placeholder="Nama siswa" name="nama" required><br>
            </div>
                
            <!-- Form mata kuliah -->
            <div class="form2">
                <label for="mapel">Mata Pelajaran :</label><br>
                <select class="form-control" id="mapel" name="mapel">
                <option value="Matematika">Matematika</option>
                <option value="Algoritma">Algoritma</option>
                <option value="Rekayasa Perangkat Lunak">Rekayasa Perangkat Lunak</option>
                <option value="Sistem Operasi">Sistem Operasi</option>
                </select><br>
            </div>

            <!-- Form Nilai-Nilai -->
            <div class="row">
                <div class="col">
                    <label for="Nilai UTS">Nilai UTS :</label><br>
                    <input type="number" class="form-control" placeholder="0" name="UTS" required> <br>
                </div>
                <div class="col">
                    <label for="Nilai UAS">Nilai UAS :</label><br>
                    <input type="number" class="form-control" placeholder="0" name="UAS" required> <br>
                </div>
                <div class="col">
                    <label for="Nilai Tugas">Nilai Tugas :</label><br>
                    <input type="number" class="form-control" placeholder="0" name="tugas" required> <br> <br>
                </div>
            </div>   
            <center>
            <input type="submit" class="btn btn-dark" name="submit" value='SUBMIT' >
            </center>
            </form><br>
        </div>
    </div>
    <!-- Output Process -->
    <?php if(isset($_POST["submit"])) : ?>
    <div class="container mt-3">
        <div class="row justify-content-center">
            <?php if($grade == "D") : ?>
                <div class="alert alert-danger">
                <?php
                echo "<h2>Grade : $grade </h2>";
                echo "Nama Siswa : $nama <br>";
                echo "Mata Pelajaran : $mapel <br>";
                echo "Nilai UTS : $uts <br>";
                echo "Nilai UAS : $uas <br>";
                echo "Nilai Tugas : $tugas <br>";
                echo "Total Nilai : $total <br>";
                ?>
            <?php elseif($grade == "C") : ?>
                <div class="alert alert-warning">
                <?php
                echo "<h2>Grade : $grade </h2>";
                echo "Nama Siswa : $nama <br>";
                echo "Mata Pelajaran : $mapel <br>";
                echo "Nilai UTS : $uts <br>";
                echo "Nilai UAS : $uas <br>";
                echo "Nilai Tugas : $tugas <br>";
                echo "Total Nilai : $total <br>";
            ?>
                </div>
            <?php else :?>
                <div class="alert alert-success">
                <?php
                    echo "<h2>Grade : $grade </h2>";
                    echo "Nama Siswa : $nama <br>";
                    echo "Mata Pelajaran : $mapel <br>";
                    echo "Nilai UTS : $uts <br>";
                    echo "Nilai UAS : $uas <br>";
                    echo "Nilai Tugas : $tugas <br>";
                    echo "Total Nilai : $total <br>";
                ?>
            </div>
            <?php endif; ?>
        </div>
    </div>
    <?php endif; ?>
</body>
</html>